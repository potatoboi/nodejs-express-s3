require('dotenv').config;
var router = require('express').Router();
var s3upload = require('../middleware/uploadToS3')
var multer  = require('multer');

const bucketName = process.env.BUCKET;
const storage = multer.diskStorage({
    destination : 'uploads/',
    filename: function (req, file, cb) {
      cb(null, file.originalname);
    }
});
const upload = multer({ storage: storage });

router.post('/',upload.single('demo_file'), (req,res) => {
  // s3upload(req.files.demo_file);
  var result =  `https://bash-test-zen.s3.amazonaws.com/${req.files.demo_file.name}`
 res.status(200).send(s3upload(req.files.demo_file));
    
})


module.exports = router;