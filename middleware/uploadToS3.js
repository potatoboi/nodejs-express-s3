var multer = require('multer');
var AWS = require('aws-sdk');

function uploadToS3(file) {
    let s3bucket = new AWS.S3({
      accessKeyId: process.env.AWSAccessKeyId,
      secretAccessKey: process.env.AWSSecretKey,
      Bucket: process.env.bucket,
    });
    s3bucket.createBucket(function () {
        var params = {
          Bucket: 'bash-test-zen',
          Key: file.name,
          Body: file.data
        };
        s3bucket.upload(params, function (err, data) {
          if (err) {
            console.log('error in callback');
            console.log(err);
          }
          console.log('success');
          return data.Location;
        });
    });
  }
  
module.exports = uploadToS3;  