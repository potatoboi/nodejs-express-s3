var express = require('express');
var app = express();
var uploadAPI = require('./routes/upload');
var busboy = require('connect-busboy');
var morgan = require('morgan');
var cors = require('cors');
const fileUpload = require('express-fileupload');

app.use(fileUpload());
app.use(cors());
app.use(express.json());
app.use(morgan('dev'));
app.use(express.json()); 
app.use(express.urlencoded({ extended: true}));
app.use(busboy()); 
app.use('/upload', uploadAPI);


app.get('/', (req,res) => {
    res.sendFile('/home/generalkenobi/Documents/TPI/node-stuff/s3-pastebin/routes/upload.html');
})

var port = process.env.PORT || 3000;

app.listen(port, (err,result) => {
    console.log(`Server is running on http://localhost:${port}`);
})